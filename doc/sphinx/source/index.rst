.. NLX_CONTROL documentation master file, created by
   sphinx-quickstart on Thu Aug  8 13:04:24 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to NLX_CONTROL's documentation!
=======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   functions

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Todos
=====

.. todolist::
