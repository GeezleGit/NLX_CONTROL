Functions
=========

.. highlight:: matlab

Topics
******

Main loop
---------

.. currentmodule:: NLX_CONTROL.SysFun

- :func:`nlx_control_cheetah`


Functions called by main loop:


Buffer objects
--------------

.. currentmodule:: NLX_CONTROL.SysFun

- :func:`nlx_control_EventBuffer`
- :func:`nlx_control_SEBuffer`
- :func:`nlx_control_CortexBuffer`
- :func:`nlx_control_CSCBuffer`

- :func:`nlx_control_BuffFlushSpike`
- :func:`nlx_control_BuffResetCortex`
- :func:`nlx_control_BuffResetEvent`
- :func:`nlx_control_BuffResetSpike`

@al_spk
-------

- :func:`nlx_control_defaultSPK`
- :func:`nlx_control_SPKaddTrial`

Utilities
---------

.. currentmodule:: NLX_CONTROL.SysFun

- :func:`nlx_control_getAnalyseDir`
- :func:`nlx_control_getDataDir`
- :func:`nlx_control_getLogDir`
- :func:`nlx_control_getMainDir`
- :func:`nlx_control_getMainWindowHandle`
- :func:`nlx_control_getSelectedAnalyses`
- :func:`nlx_control_getSettingsDir`
- :func:`nlx_control_getTrialAcqWin`

- :func:`nlx_control_checkTrial`
- :func:`nlx_control_callAnalyse`

- :func:`nlx_control_RemovePrefix`
- :func:`nlx_control_test`

.. see :func:`NLX_LoadNEV`, :func:`NLX_getEventType`::



.. - :func:`NLX_NEV2Trials`
.. .. highlight:: matlab

.. .. currentmodule:: NLXMatlabTools

.. .. autofunction:: NLX_LoadNCS()

GUI
---

.. currentmodule:: NLX_CONTROL.SysFun

- :func:`nlx_control_gui`
- :func:`nlx_control_gui_AnalyseMen`
- :func:`nlx_control_gui_SEChannelMenu`
- :func:`nlx_control_gui_CSCChannelMenu`
- :func:`nlx_control_gui_getSelectedSEChannel`
- :func:`nlx_control_gui_getSelectedCSCChannel`


.. Topic Reference
.. ***************

.. .. autofunction:: nlx_control_cheetah()

.. Buffers
.. -------

.. .. currentmodule:: NLX_CONTROL.SysFun

.. EventBuffer
.. ...........

.. .. autoclass:: nlx_control_EventBuffer

.. .. automethod:: nlx_control_EventBuffer.Ev_Flush

.. SEBuffer
.. ...........

.. .. autoclass:: nlx_control_SEBuffer

.. .. .. autoclass:: nlx_control_CortexBuffer
.. ..    :members:

.. .. .. autoclass:: nlx_control_CSCBuffer
.. ..    :members:

.. Buffer-Functions
.. ................

.. .. autofunction:: nlx_control_BuffFlushSpike
.. .. autofunction:: nlx_control_BuffResetCortex
.. .. autofunction:: nlx_control_BuffResetEvent
.. .. autofunction:: nlx_control_BuffResetSpike

Reference
*********

.. automodule:: NLX_CONTROL
   :members:

SysFun
------

.. automodule:: NLX_CONTROL.SysFun
   :members:


..    :undoc-members:

.. ..    :show-inheritance:
